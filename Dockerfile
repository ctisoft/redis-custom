FROM redis:3.0.7
COPY run-redis.sh /usr/local/bin/
COPY redis.conf /etc/
CMD [ "sh", "/usr/local/bin/run-redis.sh" ]
